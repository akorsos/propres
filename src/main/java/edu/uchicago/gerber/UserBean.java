/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

//Webpage>jsfintro
//Webpage>SourcePackage

@Named (value="userBean")
@ManagedBean(name="userBean")
@RequestScoped
public class UserBean {

 private String name;

    //getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
    
    
    //method to do something in our application
    public String saveUser(){
        
        System.out.println("do work here");
        //example of work to be done
        //save to database
        //then send a confirmation email to the user
        
        //return the name of the confirmation page
        return "confirm.xhtml";
    }
  
    
}
